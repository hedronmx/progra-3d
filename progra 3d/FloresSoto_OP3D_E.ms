
-- Carlos Ignaco Flores Soto
-- 12/04/2018
-- Primer Parcial Examen - Manejo Basico de Max Script

-- CLASSOF Regresa la clase de un objeto. Necesitas darle un valor
-- GC() Garbage Collector, Borra memoria. No ocupa valor
-- name: "nombre" -- nombre el objeto
-- pos: [x, contador z] -- cambia la posicion del objeto
-- show $nombredeobjeto -- muestra las propiedades del objeto
-- delete $nombredeobjeto --elimina obejtos
-- recenter -- Centra el objeto
-- pivot= [x, contador, z] -- modifica el pivote de un objeto (x, contador z)
-- roation =(eulerAngeles 45 0 0 as quat)



--Todo esto es para seleccionar cosas dentro de la jerarquia
-- objects
-- selection
-- geometry
-- ahelpers
-- cameras
-- lights
-- spacewarps
-- system
-- shapes

-- Todo esto es para crear cosas
-- cube() --crea un cubo
-- sphere() -- crea una esfera
-- omnilight() -- crea una luz omni
-- freecamera() -- crea una camara libre
-- circle() -- crea un spline de circulo

-- Modificadores de Objetos
-- $.nombresize=[x,contador,z] --Modifica el tamanio del obejto
-- $.wirecolor = [color] --cambia el color del wireframe
-- $.nombrename= "nombre" --cambia el nombre del obejto

-- A=#(1, "string", color 255 255 34) -- arreglo de 3 espacios
-- A=#() -- arreglo vacio
-- A.count -- regresa tama�o
-- join A ("E") -- Agrega un nuevo objeto o arrelgo en el arreglo
-- append A[posicion] objeto -- agrega una variable en x posicion de un arreglo
-- deleteitem A posicion --Borra elemento de x posicion
-- finditem A valor -- encuentra dentro de un arreglo

-- FOR i=1 to x DO codigo

OpenLog "C:\Users\pope_\Documents\Tridimin\FloresCarlos_OP3D_E.txt" mode:"a" outputonly:false

cajas = #("C1", "C2", "C3");
teteras = #("T1", "T2", "T3");
omni = #("L1", "L2", "L3");

argb = #(RED, GREEN, BLUE);
arreglo = #(color 0 255 255, color 255 0 255, color 255 255 0);

ahelpers = #("H1", "H2", "H3");
contador = 0;
temporal1=0;
temporal2=0;
circulos = #("C1", "C2", "C3");

num = #();
temporal = 0;

for i=1 to cajas.count do (
	box name: cajas[i] pos: [0, contador, 0] wirecolor: argb[i];
	teapot name: teteras[i] pos: [75, contador, 0] wirecolor: arreglo[i]; radius:15;
	omnilight name: omni[i] pos: [30, contador, 0] wirecolor: arreglo[i];
	point name: ahelpers[i]   pos: [-30, contador, 0] wirecolor: argb[i];
	circle name: circulos[i] pos: [120, contador, 0] radius: 3 wirecolor: arreglo[i];
	contador=contador+45;
)
cajax = for i in geometry where classof i == box collect i;
teterax = for i in geometry where classof i == teapot collect i;
helperx = for i in objects where classOf i == point collect i;
omnix = for i in objects where classOf i == omnilight collect i;
circulox = for i in objects where (classof i == circle and classof i == teapot ) collect i;

for i = 1 to 3 do(
	append num i;
	for j = i to i do
		append num j;
);
geometria = for i in geometry where i.wirecolor == RED or i.wirecolor == GREEN or i.wirecolor == BLUE collect i;
puntos = for i in objects where classof i == point collect i;
for i = 1 to 3 do(
	temporal1 = teapot name: "T" pos:[cajax[i].center.x, cajax[i].center.y, cajax[i].max.z] wirecolor: arreglo[i] radius: 6;
	temporal2 = box name: "C" wirecolor: argb[i] pos:[teterax[i].center.x, teterax[i].center.y, teterax[i].max.z] height: 6 width: 6 length: 6;
	temporal1.pivot = teterax[i].pivot;
	temporal2.pivot = cajax[i].pivot;
)
for i = 1 to geometria.count do(if geometria[i].wirecolor == RED
			then geometria[i].parent = puntos[1]
	else if geometria[i].wirecolor == GREEN
	 		then geometria[i].parent = puntos[2]
	else if geometria[i].wirecolor == BLUE
	then geometria[i].parent = puntos[3])

-- Abajo de esto, puedo usar solo omnilight collect i ??

teteritas = for i in geometry where classof i == teapot and i.radius == 6 collect i
antitetera = for i in objects where (i.wirecolor == arreglo[1] or i.wirecolor == arreglo[2] or i.wirecolor == arreglo[3]) and i.radius != 6 collect i
antiluces = for i in objects where classof i != omnilight collect i
lucex = for i in objects where classof i == omnilight collect i

-- Hasta aqui
for i = 1 to antitetera.count do
(
	 temporal = num[i]
	if antitetera[i].wirecolor == teteritas[temporal].wirecolor then antitetera[i].parent = teteritas[temporal]
)
for i = 1 to lucex.count do(
	lucex[i].pos.z = 0.8 * (antiluces[random 1 antiluces.count].pos.x + antiluces[random 1 antiluces.count].pos.y)
)
superarreglo = for i in objects where i.children.count > 1 collect i
for i = 1 to superarreglo.count do rotate superarreglo[i] (eulerAngles 0 0 45)

format "NOMBRE\t\tTIPO\t\tCOLOR\t\t#HIJOS\t\tPOSICION\t\t\t\tROTACION\n"
for i = 1 to objects.count do (
	if classof objects[i] == point or classof objects[i] == teapot or classof objects[i] == circle then(
	format "%\t\t%\t\t  %\t\t%\t\t%\t\t\t\t%\n" objects[i].name objects[i].name objects[i].wirecolor objects[i].children.count objects[i].pos objects[i].rotation)
)
for i = 1 to objects.count do (
	if classof objects[i] == box or classof objects[i] == omnilight then(
	format "%\t\t%\t\t  %\t\t%\t\t%\t\t\t\t%\n" objects[i].name objects[i].name objects[i].wirecolor objects[i].children.count objects[i].pos objects[i].rotation)
)

closelog();
