openlog "C:\Users\pope_\Documents\tridimin\FLORESCARLOS_OP3D_A.txt" mode: "a" --Abre un log y lo crea

-- Carlos Ignacio Flores Soto
-- Actividades 4 a 8 / Abril 25 del 2018

-- ACTIVIDAD 5
-- MOVER LA ESFERA 10 UNIDADES EN Z
-- nombre.pos.z
Esfera= sphere name:"E1" radius:10 pos:[0,0,25] wirecolor:[255,255,0]
Esfera.pos.z+=10

-- ACTIVIDAD 6
-- CREAR UNA CAJA CON EL NOMBRE C0 CON LAS DIMENSIONES DE 5 UNIDADES Y POSICIONARLA EN EL PUNTO MÁXIMO DE LA CAJA C1 CON EL COLOR AMARILLO
-- nombre.max.z nombre.max.x nombre.max.y

Caja=box name:"CAJA1" width:25 length:25 wirecolor:RED
Caja.name= "C1"
Cajax=box name:"C0" width:5 length:5 wirecolor:[255,255,0] pos:[Caja.max.x, Caja.max.y, Caja.max.z]

-- ACTIVIDAD 7
-- HACER UNA CAJA DE LOS COLORES DE RGB Y CMY QUE ESTÉN EN CADA UNA DE LAS
-- COORDENADAS DE LAS PROPIEDADES DE LA CAJA C1, CON EL NOMBRE ASIGNADO DE LA POSICIÓN
-- QUE TOMARAN ÓSEA EN SU PUNTO MAX EXISTIRÁ UNA CAJA CON EL NOMBRE MAX Y EL WIRECOLOR ROJO,
-- MIN COLOR VERDE EN LA POSICIÓN MIN DE LA CAJA C1, CENTER COLOR AZUL EN LA POSICIÓN CENTRAL DE
-- C1, PIVOT CON EL COLOR CIAN EN LA POSICIÓN DEL PIVOTE DE C1, Y UNA CAJA NOMBRE POSITION DE
-- 2 UNIDADES EN SUS DIMENSIONES CON EL COLOR MAGENTA EN LA POSICIÓN DE C1
-- nombre.max.z nombre.min.x nombre.center.y nombre.pivot.z

Cajared=box name:"Max" width:5 length:5 wirecolor:RED pos:[Caja.max.x, Caja.max.y, Caja.max.z]

Cajagreen=box name:"Min" width:5 length:5 wirecolor:Green pos:[Caja.min.x, Caja.min.y, Caja.min.z]

Cajablue=box name:"Center" width:5 length:5 wirecolor:Blue pos:[Caja.center.x, Caja.center.y, Caja.center.z]

Cajacian=box name:"Pivot" width:5 length:5 wirecolor:[0, 255, 255] pos:[Caja.pivot.x, Caja.pivot.y, Caja.pivot.z]

Cajamagenta=box name:"Position" width:5 length:5 wirecolor:[255,0, 255] pos:[Caja.pos.x + 2, Caja.pos.y + 2, Caja.pos.z + 2]


-- ACTIVIDAD 8
-- DE LOS OBJETOS CREADOS IMPRIMIR POR LÍNEA EL COLOR DE CADA
-- UNO DE LOS OBJETOS CON EL FORMATO "NOMBRE OBJETO:" NOMBRE
-- OBJETO " COLOR: " COLOR DEL OBJETO
-- FOR i=1 to x DO codigo
-- A=#() -- arreglo vacio
-- A.count -- regresa tamaño
arreglo = for i in objects collect i
for i = 1 to arreglo.count do
format "Nombre Objeto:% Color:%\n" arreglo[i].name arreglo[i].wirecolor
