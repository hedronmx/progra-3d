openlog "C:\Users\pope_\Documents\Tridimin\FLORESCARLOS_OP3D_A_1.txt" mode: "a" --Abre un log y lo crea

-- Carlos Ignacio Flores Soto
-- Actividades Mayo 3 del 2018

-- ACTIVIDAD 1
-- Clasificar en un arreglo llamado Ep Todas las geometrias editable polu
-- clasificar en un arreglo llamado EM a todas las geometrias editables mesh
-- clasificar en un arreglo llamado prim todas las geometrias editables primitivas
EM= #()
EP= #()
Prim= #()
geometria = for i in geometry collect i;
if classof geometria[i] == Editable_Mesh then append EM geometria[i];
else if classof geometria[i] == Editable_Poly then append EP geometria[i];
else append Prim geometria [i];

closelog() --cierra archivo

openlog "C:\Users\pope_\Documents\Tridimin\FLORESCARLOS_OP3D_A_2.txt" mode: "a" --Abre un log y lo crea

-- ACTIVIDAD 2
-- Clasificar en un arreglo llamado EP Todas las geometrias editable polu
-- clasificar en un arreglo llamado EM a todas las geometrias editables mesh
-- clasificar en un arreglo llamado prim todas las geometrias editables primitivas

Temporal = #()
Temporal2 = #()
Temporal3 = #()

for i=1 to EM.count do append Temporal (GETPOLYGONCOUNT EM[i])[1]
for i=1 to EP.count do append Temporal2 (GETPOLYGONCOUNT EP[i])[1]
for i=1 to Prim.count do append Temporal3 (GETPOLYGONCOUNT Prim[i])[1]

Minimo = amin Temporal
Minimo2 = amin Temporal2
Minimo3 = amin Temporal3

closelog() --cierra archivo