/*
Flores Soto Carlos Ignacio
22-03-2018
*/
--Se abre el log
openlog "C:\Users\carlos\Documents\tridimin\log220318.txt" mode: "a"

--Se crean objetos
box name: "b1" wirecolor: red pos: [0,0,0]
box name: "b2" wirecolor: green pos: [30,0,0]
box name: "b3" wirecolor: blue pos: [60,0,0]
box name: "b4" wirecolor: [0,255,255] pos: [90,0,0]

point name: "p1" wirecolor: red pos: [0,0,0]
point name: "p2" wirecolor: green pos: [0,30,0]
point name: "p3" wirecolor: blue pos: [0,60,0]
point name: "p4" wirecolor: [0,255,255] pos: [0,90,0]

freecamera name: "c1" wirecolor: red pos: [0,30,0]
freecamera name: "c2" wirecolor: green pos: [0,60,0]
freecamera name: "c3" wirecolor: blue pos: [0,120,0]
freecamera name: "c4" wirecolor: [0,255,255] pos: [0,180,0]

omnilight name: "l1" wirecolor: red pos: [0,60,0]
omnilight name: "l2" wirecolor: green pos: [0,120,0]
omnilight name: "l3" wirecolor: blue pos: [0,180,0]
omnilight name: "l4" wirecolor: [0,255,255] pos: [0,210,0]

--Información de props
helpers=getpropnames $p1
camara=getpropnames $c1
cajas=getpropnames $b1
luces=getpropnames $l1

--se cierra el log
closelog()